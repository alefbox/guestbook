<?php

namespace App\Repository;

use App\Entity\Message;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

class MessageRepository extends ServiceEntityRepository
{
	/**
	 *
	 */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Message::class);
    }

	/** 
	 * получить сообщения с учетом постранички
	 */	
    public function getPageOfMessages(int $page=1)
    {
        $DQL = $this->getEntityManager()->createQuery("
            SELECT m FROM App:Message m 
			WHERE m.status = '".Message::STATUS_ACTIVE."' 
            ORDER BY m.createdAt DESC
        ");

        return $DQL->setMaxResults(Message::MAX_PER_PAGE)->setFirstResult(($page-1)*Message::MAX_PER_PAGE)->getResult();
    }
	
	/**
	 * создать новое сообщение
	 */	
    public function createMessage(string $username, string $email, string $text)
    {
        $Message = (new Message())
			->setUsername($username)
			->setEmail($email)
			->setText($text)
			->setStatus(Message::STATUS_ACTIVE);
			
		$this->getEntityManager()->persist($Message);
		$this->getEntityManager()->flush();
			
		return $Message;	
    }	
}
