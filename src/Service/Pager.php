<?php
/**
 * Pager
 * 
 * 
 * @author alefbox <alefbox@gmail.com>
 * @version 1.0
 */

namespace App\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;

use Twig_Environment as Environment;

use App\Entity\Message;

class Pager
{
	private $twig;	
	private $options;
	private $router;
	protected $em;
	
	/**
     * $twig
     */
    public function __construct(EntityManagerInterface $entityManager, Environment $twig, UrlGeneratorInterface $router)
    {
		$this->em = $entityManager;
		$this->twig = $twig;
		$this->router = $router;
		
		$this->options = array(
			'all' => 0,
			'page' => 1,
			'max_result' => 1,
			'count' => 0,
			'path' => '',
			'render' => '',
		);		
    }
	
	/**
	 *
	 */
	public function getMessagePager($page)
	{
		$this->options['page'] = $page;
		$this->options['path'] = $this->router->generate('_messages_block', ['page' => '111111']);		
        $this->options['max_result'] = Message::MAX_PER_PAGE;
        
        // Получаем количество записей
        $this->options['all'] = $this->em->createQuery("SELECT count(m.id) FROM App\Entity\Message m")->getResult(\Doctrine\ORM\Query::HYDRATE_SINGLE_SCALAR);
        
        // Получаем количество страниц
        $this->options['count'] = ceil($this->options['all']/$this->options['max_result']);		
		$this->options['render'] = $this->twig->render('pager/pager.html.twig', ['pager' => $this->options]);
		
		return $this->options;
	}
}
