<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use App\Entity\Message;
use App\Service\Pager;

class MessageController extends Controller
{
    /**
     * @Route("/message/add" , name="_message_add")
     */
    public function addAction(Request $request)
    {        
		$ret = 'ok';
		
		$username = trim($request->get('username'));
		$email = trim($request->get('email'));
		$text = trim($request->get('text'));
		
		if (!$username) $ret = 'Укажите ваше имя';
		if (!$email) $ret = 'Укажите ваш email';
		if (!$text) $ret = 'Укажите текст сообщения';
		
		if (mb_strlen($username) > 255) $username = mb_substr($username, 255);
		if (mb_strlen($email) > 255) $email = mb_substr($email, 255);
		
		// проверяем валидность email
		$emailConstraint = new Email();
		$emailConstraint->message = 'Указан неверный email';	
		$errorList = $this->get('validator')->validate($email, $emailConstraint);
		
		if (count($errorList)) $ret = $emailConstraint->message;
		
		if ($ret == 'ok') {
			$em = $this->getDoctrine()->getManager();
			$Message = $em->getRepository(Message::class)->createMessage($username, $email, $text);
		}	
		
        return new Response($ret);
    }
	
    /**
     * @Route("/messages/block/{page}" , name="_messages_block", requirements={"page"="\d+"})
     */
    public function blockAction(Pager $pager, $page=1)
    {
        $em = $this->getDoctrine()->getManager();
		$Messages = $em->getRepository(Message::class)->getPageOfMessages($page);		
		
        return $this->render('message/messages.block.html.twig', [
			'Messages' => $Messages, 
			'pager' => $pager->getMessagePager($page)
		]);
    }	
}
