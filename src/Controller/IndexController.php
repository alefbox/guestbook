<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use App\Entity\Message;
use App\Service\Pager;

class IndexController extends Controller
{
    /**
     * @Route("/" , name="_index_page")
     */
    public function indexAction(Pager $pager)
    {
        return $this->render('index/index.html.twig', []);		
    }
}
